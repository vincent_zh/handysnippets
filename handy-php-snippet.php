<?php

/**
 * Benchmark
 * http://www.php.net//manual/en/function.microtime.php
 *
 * Note that this doesn't measure the time of cpu the script actually burned, 
 * rather just the time that passed between the start and the end. 
 * This might give you very inaccurate results sometimes, 
 * because this time includes the time spent on all the other work during that time, 
 * like system calls, interruptions, other processes, etc. 
 * In a situation, where this script runs alone once on an iddle server, 
 * this gives you somewhat accurate idea of how resource-intensive the script is. 
 * But imagine a 1000 requests for this script at the same moment, 
 * then all those 1000 processes would measure 1000 times longer execution time, 
 * because it merely measures the overall time passed. 
 * In other words, this kind of stopwatch doesn't pause when multitasking 
 * kicks in and suspends the process.
 * - from http://www.developerfusion.com/code/2058/determine-execution-time-in-php/
 *
 */
$time_start = microtime(true);
// do something here
$time_end = microtime(true);
$time = $time_end - $time_start;
echo "Execution in $time seconds\n";
/**************************************************************/

/**
 * Stopping execution for a while 
 * http://www.php.net//manual/en/function.sleep.php
 * 
 * http://stackoverflow.com/questions/6730855/php-wait-5-seconds-before-executing-an-action
 */
sleep(NUMBER_OF_SECONDS);
// or
usleep(NUMBER_OF_MICRO_SECONDS);
/**************************************************************/



/**
 * Date & Time
 * 
 * In PHP (actually, all the computer languages), datetime can be represented in three ways:   
 * 1. string of the datetime;
 * 2. time-stamp, which is indeed an integer, representing the time measured in the number of seconds 
 *    since the Unix Epoch (January 1 1970 00:00:00 GMT) 
 * 3. a didecated class instance to represent a certain datetime, which means that the
      class encapsulate all the operations & data of datetime in an abstract lelvel. 
      In PHP context, since 5.2.0 version, a class called DateTime is introduced for that purpose: 
      http://php.net/manual/en/class.datetime.php
 *
 * Generally, data of datetime is stored and transfered in the first format, namely string. However, 
 * the arithmetic calculation is always performed with the second representation.
 *
 * So in general, we need two kinds of capbilities to deal with datetime:
 *
 * 1. convert between string and timestamp representation
 * 2. do arithmetic operation against timestamp
 *
 */


/*
 * Number 1: let's examine how to parse a datetime string 
 * PHP provides some function to parse a string, and return a time-stamp:
 */




/*
 * strtotime() http://php.net/manual/en/function.strtotime.php
 */

echo strtotime("05 August 2014") . "\n";    // 1407168000

/*
 * But it is designed to work mainly in another way, and that is the most powerful feature of this function.
 * It allows you to work out the time-stamp based on a string describing a period of time, relating
 * to a time specified by the second parameter, and it is a time-stamp, if you omit it, the default value will
 * be the return value of time(), which is the current time of the execution.
 */

echo strtotime("+1 day") . "\n";
/* it is equivalent to */

echo strtotime("+1 day", time()) . "\n";
/*
 * You are not limited to use 'day', you can use "+1 week", and etc..
 *
 */




/*
 * mktime() http://php.net/manual/en/function.mktime.php
 * Refer to Example #2 for this function's versatility, as well as mis-input camptablity
 */
echo mktime(0, 0, 0, 8, 5, 2014) . "\n";    // 1407168000


/*
 * Number 2: the next up is how to format datetime string before outputting. 
 * PHP provides some function to format a datetime, and return a string:
 */

/*
 * date() http://php.net/manual/en/function.date.php
 * 
 */

echo date("Y-m-d", time());




/*
 * The DateTime class 
 * http://php.net/manual/en/class.datetime.php
 */

/* get time-stamp */
$oneday = new DateTime("2014-08-05 00:00:00.0000");
echo $oneday->getTimestamp() . "\n";	//1407168000

$oneday_s = new DateTime("2014-08-05");
echo $oneday_s->getTimestamp() . "\n";	//1407168000

/* date_create() */

/*
 * All in all, what sort of datetime string representation can be understood
 * by the above mentioned functions, and in turn be parsed to a time.
 * Refer to: http://php.net/manual/en/datetime.formats.php
 */



/**************************************************************/


/**
 * Set Cookie
 * http://stackoverflow.com/questions/612034/how-can-i-set-a-cookie-and-then-redirect-in-php
 * 
 */

setcookie("key", $value, time()+3600, "/subpath/sub-subpath/", "domainname.com.hk");


/**************************************************************/

/**
 * Redirect
 * http://stackoverflow.com/questions/768431/how-to-make-a-redirect-in-php
 * 
 */

redirect('redirectTo.php');

function redirect($url, $statusCode = 303)
{
	header('Location: ' . $url, true, $statusCode);
	die();
}


/**************************************************************/

/**
 * Filters
 * http://sg3.php.net/manual/en/filter.filters.misc.php
 */
 
function trimString($value)
{
	return trim($value);
}


$loginname = filter_input(INPUT_POST, 'loginname', FILTER_CALLBACK, array('options' => 'trimString'));


/**************************************************************/

/**
 * Access WEB services with CURL
 * 
 */
 
 
define("COOKIE_FILE", dirname(__FILE__) . "\\cookie.txt");
define('IS_HTTP_AUT', false);

$CONFIG['BASIC_AUTHEN'] = 'opentide:dmg1141';
$CONFIG['HOST_ADDR'] = 'http://domainname.com';
$CONFIG['REFERER'] = 'http://localhost/';

$queryString = array(
	"token" => "",
	"limit" => 10,
	"skip" => 100,
	"sort" => "id+asc"
);

/*
 * POST request
 */
$options = array(
	CURLOPT_URL => $CONFIG['HOST_ADDR'],
	CURLOPT_POST => true,
	CURLOPT_REFERER => $CONFIG['REFERER'],
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_COOKIESESSION => true,	// this field is set only for the first request, in the consequent ones, you must remove it
	CURLOPT_COOKIEJAR => COOKIE_FILE,
	CURLOPT_COOKIEFILE => COOKIE_FILE,
	CURLOPT_POSTFIELDS => http_build_query($this->_loginFields)	// for post request
);

/*
 * GET request
 */
$options = array(
	CURLOPT_URL => $CONFIG['HOST_ADDR'] . '?' . http_build_query($queryString),	// append this for get request
	CURLOPT_REFERER => $CONFIG['REFERER'],
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_COOKIESESSION => true,	// this field is set only for the first request, in the consequent ones, you must remove it
	CURLOPT_COOKIEJAR => COOKIE_FILE,
	CURLOPT_COOKIEFILE => COOKIE_FILE,
);

/*
 * requests going with COOKIE in place
 */
$options = array(
	CURLOPT_URL => $CONFIG['HOST_ADDR'] . '?' . http_build_query($queryString),	// append this for get request
	CURLOPT_REFERER => $CONFIG['REFERER'],
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_COOKIEJAR => COOKIE_FILE,
	CURLOPT_COOKIEFILE => COOKIE_FILE,
);


if (IS_HTTP_AUT)
{
	$options[CURLOPT_USERPWD] = $CONFIG['BASIC_AUTHEN'];
}


$curl = curl_init();
curl_setopt_array($curl, $options);
$result = curl_exec($curl);

if ($result!=NULL && $result != '')
{
	// operate the result here
	curl_close($curl);
}
else
{
	// log
	curl_close($curl);
}

/**************************************************************/


