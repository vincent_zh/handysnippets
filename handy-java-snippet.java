List<Employee> eList = new ArrayList<Employee>() {
    {
        add(new SalesPerson(0, "Brooklyn", "Lee", PositionType.SALESPERSON));
        add(new OtherStaff(0, "Flower", "Tucci", PositionType.OTHERS));
				add(new SalesPerson(0, "Leilani", "Leeane", PositionType.SALESPERSON));
				add(new SalesPerson(0, "Lizz", "Tayler", PositionType.SALESPERSON));
				add(new OtherStaff(0, "Justine", "Joli", PositionType.OTHERS));
				add(new SalesPerson(0, "Bibi", "Jones", PositionType.SALESPERSON));
    }
};

for (Employee temp : eList) {
	System.out.println(temp);
}
