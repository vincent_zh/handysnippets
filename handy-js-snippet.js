/*
 * Logging to console, the key is that IE8 doesn't support console, you must check it
 * before using it, so that you can make your code cross-browser.
 */
if (window.console) {
	console.log("Label");
	console.log(obj);
}
/********************************************/


/*
 * Iterating through object's properties
 *  
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in

 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach
 */

var vince = {};
vince['keyName1'] = 2;
vince['keyName2'] = 3;
vince['keyName3'] = 4;
vince['keyName4'] = 5;
vince['keyName5'] = 6;

console.log(vince.keyName1);

for (var key in vince) {
	console.log(vince[key]);
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for_each...in
for each (var item in vince) {
	console.log(item);
}

/*
 * http://stackoverflow.com/questions/921789/how-to-loop-through-javascript-object-literal-with-objects-as-members
 * http://stackoverflow.com/questions/684672/loop-through-javascript-object
 * http://stackoverflow.com/questions/8312459/iterate-through-object-properties
 */
for (var key in validation_messages) {
	var obj = validation_messages[key];
	for (var prop in obj) {
		// important check that this is objects own property 
		// not from prototype prop inherited
		if(obj.hasOwnProperty(prop)){
			alert(prop + " = " + obj[prop]);
		}
	}
}

/********************************************/

/*
 * Datetime
 */

var parse_date_without_time = getDateFromFormat('2014-08-05', 'yyyy-MM-dd');
console.log(parse_date_without_time);   // 1407204583000


var parse_date_with_time = getDateFromFormat('2014-08-05 00:00:00', 'yyyy-MM-dd HH:mm:ss');
console.log(parse_date_with_time);   // 1407168000000

parse_date_without_time - parse_date_with_time;   // 36583000, 

var d0805 = new Date();

d0805.setTime(1407204583000);
d0805.toGMTString();    // "Tue, 05 Aug 2014 02:09:43 GMT"
d0805.toLocaleString(); // "8/5/2014 10:09:43 AM"
d0805.toString();   // "Tue Aug 05 2014 10:09:43 GMT+0800 (China Standard Time)"

d0805.setTime(1407168000000);
d0805.toGMTString();    // "Mon, 04 Aug 2014 16:00:00 GMT"
d0805.toLocaleString(); // "8/5/2014 12:00:00 AM", this is actually the midnight between 4th Aug and 5th Aug, the start of 5th Aug 
d0805.toString();   // "Tue Aug 05 2014 00:00:00 GMT+0800 (China Standard Time)"

// http://en.wikipedia.org/wiki/24-hour_clock
// http://en.wikipedia.org/wiki/12-hour_clock#Confusion_at_noon_and_midnight

/*
 * so the function is mal-functioning, not as our expect at least, actually, 
 * only parse_date_with_time is relaiable now.
 */


var timestamp = Date.parse("2014-08-08T18:06:52.49");   // 1407521212490, here we use the time format adopted by MS SQL Server

var regDate = new Date();
regDate.setTime(timestamp);

regDate.toGMTString();  // "Fri, 08 Aug 2014 18:06:52 GMT"
regDate.toLocaleString();   // "8/9/2014 2:06:52 AM"

regDate.getTimezoneOffset();    // -480

var real_timestamp = timestamp + regDate.getTimezoneOffset() * 60 * 1000;   // 1407492412490

var real_regDate = new Date();
real_regDate.setTime(real_timestamp);

real_regDate.toGMTString(); // "Fri, 08 Aug 2014 10:06:52 GMT"
real_regDate.toLocaleString();  // "8/8/2014 6:06:52 PM", Yeah, this is it!


// http://www.comptechdoc.org/independent/web/cgi/javamanual/javadate.html

var timestamp = Date.parse("2014-08-08 18:06:52.49");   // 1407492412490

var regDate = new Date();
regDate.setTime(timestamp);

regDate.toGMTString();  // "Fri, 08 Aug 2014 10:06:52 GMT"
regDate.toLocaleString();   // "8/8/2014 6:06:52 PM"


/********************************************/





/*
 * Use Ajax with Promise & Deferred in jQuery
 * 
 */

function loginSuccedHandler (_res) {
	if (_res.status == '200') {
		var role = _res.role;
		var username = _res.username;
		window.location.href = "main.html#/?name="+username+"&role="+role;
	}
}

function loginFailHandler (_req, _stat, _err) {
	alert('Network Connection Error! Please contact network administrator.');
}

var loginname = $("#inpt-username").val();
var password = $("#inpt-password").val();

var obj = {
	password: CryptoJS.SHA3(password, { outputLength: 256 }).toString(),
	login: loginname
}

/* create an Ajax promise */
var loginPromise = $.ajax({
	type: "POST",
	url: "/api/login",
	data: obj
});

/* arrange its handlers */
loginPromise.then(loginSuccedHandler, loginFailHandler);

