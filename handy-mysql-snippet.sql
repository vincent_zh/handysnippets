-------------------
------ M.I.T. -----
-------------------
SELECT `c`.`Id`, `EmployeeId`, `Name`, `PaymentMethod`,
		CONCAT(`e`.`FirstName`, ' ', `e`.`LastName`) AS `AgentName`
FROM `customer` `c` INNER JOIN `employee` `e`
ON `c`.`EmployeeId` = `e`.`Id`

INSERT INTO `employee`(`FirstName`, `LastName`, `Position`) VALUES ('Vic','Stone','Others');

INSERT INTO `customer`(`EmployeeId`, `Name`, `PaymentMethod`) SELECT `Id`, 'Merlyn',  'Cash' FROM `employee` WHERE `FirstName` = 'Oliver';

SELECT `c`.`Id`, `EmployeeId`, `Name`, `PaymentMethod`, IFNULL(CONCAT(`e`.`FirstName`, ' ', `e`.`LastName`), '') AS `AgentName` FROM `customer` `c` LEFT JOIN `employee` `e` ON `c`.`EmployeeId` = `e`.`Id` WHERE `c`.`Id` = 15 ORDER BY `EmployeeId`, c.`Id`

-- JOIN AGGREGATE

-- method 1, join
SELECT e.Id, e.FirstName, e.LastName,
COUNT(c.EmployeeId) AS no_of_customers
FROM employee AS e
LEFT JOIN customer AS c ON e.Id = c.EmployeeId
GROUP BY e.Id

-- method 2, coorelated sub-query
SELECT e.FirstName, ( SELECT COUNT(*) FROM customer AS c WHERE c.EmployeeId = e.Id ) AS no_of_customers FROM employee AS e

-- method 3




-------------------
------ CHEIL -----
-------------------
-- to match string with wildcard of one single character
SELECT * FROM `model` WHERE `model_code` LIKE '1000_'


-- merge two record sets with UNION
SELECT SUM(`point_used`) AS point, "Redemption" AS point_type FROM `user_redeem` WHERE `lastModifiedTime` > '2014-07-01'
UNION
SELECT SUM(`point_gain`) AS point, "ProdReg" AS point_type FROM `product_registration` WHERE `record_datetime` > '2014-07-01'

-- to create temporary table
CREATE TEMPORARY TABLE IF NOT EXISTS table2 AS (
    SELECT * FROM table1
)

-- to create temporary table
-- with string processing
-- with outer join
-- and compare column with NULL value
CREATE TEMPORARY TABLE IF NOT EXISTS `user_warranty` AS (
	SELECT LOWER(w.`email`) AS `WEmail`, LOWER(u.`email`) AS `UEmail`
	FROM `warranty` w LEFT JOIN `user` u ON LOWER(w.`email`) = LOWER(u.`email`)
	WHERE w.`record_date` < '2014-07-14' AND w.`record_date` > '2014-07-07'
);

SELECT * FROM `user_warranty` uw WHERE `UEmail` IS NOT NULL;

-- to create stored procedure

DELIMITER $$

DROP PROCEDURE IF EXISTS `GetAllProducts`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetAllProducts`()
BEGIN

SELECT SUM( statuuu.num_of_real_coupon ) AS coupon, statuuu.redeem_item_id
FROM (

SELECT ur.id, ur.quantity, ur.point_used, ri.point_required, ri.face_value, ri.unit_factor, ri.id AS redeem_item_id, ur.quantity * ri.unit_factor AS num_of_real_coupon
FROM  `user_redeem` ur
INNER JOIN  `redeem_item` ri ON ur.redeem_item_id = ri.id
WHERE  `lastMTime` >=  '2014-05-24'
AND  `lastMTime` <=  '2014-06-24'
AND  `status` =3
) AS statuuu
GROUP BY redeem_item_id;

END$$

DELIMITER ;





SELECT DISTINCT `sua_id` FROM `product_registration`
WHERE
(
	`model_name` LIKE '%UA46%' OR
	`model_name` LIKE '%UA48%' OR
	`model_name` LIKE '%UA5%' OR
	`model_name` LIKE '%UA6%' OR
	`model_name` LIKE '%UA7%' OR
	`model_name` LIKE '%UA8%' OR
	`model_name` LIKE '%UA9%' OR
	`model_name` LIKE '%UA10%' OR
	`model_name` LIKE '%UA11%'
	)
 AND `sua_id` IN (
					SELECT `sua_id` FROM `product_registration` WHERE `model_name` LIKE '%GALAXY NOTE%'
				)
-- 160


SELECT DISTINCT pr1.`sua_id` FROM
`product_registration` pr1 INNER JOIN
`product_registration` pr2 ON pr1.`sua_id` = pr2.`sua_id`
WHERE
(
	pr1.`model_name` LIKE '%UA46%' OR
	pr1.`model_name` LIKE '%UA48%' OR
	pr1.`model_name` LIKE '%UA5%' OR
	pr1.`model_name` LIKE '%UA6%' OR
	pr1.`model_name` LIKE '%UA7%' OR
	pr1.`model_name` LIKE '%UA8%' OR
	pr1.`model_name` LIKE '%UA9%' OR
	pr1.`model_name` LIKE '%UA10%' OR
	pr1.`model_name` LIKE '%UA11%'
)
AND pr2.`model_name` LIKE '%GALAXY NOTE%'
-- 160
