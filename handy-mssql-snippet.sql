/************************************************************
*************************************************************
****************** FUNDAMENTAL FUNCTIONS ********************
*************************************************************
************************************************************/

------------------------------------------------------------
-------------------- M.I.T EXAM --------------------
------------------------------------------------------------

-- compare ntext with varchar literal
--1.
CAST()/CONVERT()

--2.
N'literal'

--3.
LIKE






------------------------------------------------------------
-------------------- Table manipulation --------------------
------------------------------------------------------------


-- how to create a table
CREATE TABLE [dbo].[TableName](
	[id] [INT] IDENTITY(1,1) NOT NULL,
	[guid] [VARCHAR](50) NOT NULL,
	[first_name] [nvarchar](255) NULL,
	[phone] [nvarchar](255) NULL
 CONSTRAINT [PK_TableName] PRIMARY KEY CLUSTERED
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
------------------------------------------------------------


-- how to drop a table
DROP TABLE [dbo].[audit_log];
-- check before dropping
IF OBJECT_ID('dbo.campaign_offer', 'U') IS NOT NULL
	DROP TABLE dbo.campaign_offer;
------------------------------------------------------------


-- how to add one column to a table
ALTER TABLE {TABLENAME}
ADD {COLUMNNAME} {TYPE} {NULL|NOT NULL}
CONSTRAINT {CONSTRAINT_NAME} DEFAULT {DEFAULT_VALUE}
[WITH VALUES]
------------------------------------------------------------


-- how to drop one column from a table
ALTER TABLE table1 DROP COLUMN field1;
------------------------------------------------------------



------------------------------------------------------------
----------------------- Logic Control ----------------------
------------------------------------------------------------


-- CASE WHEN THEN ELSE
SELECT d.name,
CASE
	WHEN SUM(e.salary) IS NULL THEN 0
	ELSE SUM(e.salary)
END AS TotSal
FROM departments d LEFT JOIN employees e
ON d.id = e.department_id
GROUP BY d.name
------------------------------------------------------------


-- it will stop searching immediately once a condition is meeted
DECLARE @cond1 int
		,@cond2 int

SELECT @cond1 = 0
		,@cond2 = 0

SELECT
CDGV =
		CASE
			WHEN @cond1 = 0 THEN 'condition 1'
			WHEN @cond2 = 0 THEN 'wont be here'
		END
------------------------------------------------------------


--SET VS. SELECT
--SET merely works for one variable
--SELECT works for multiple variables
DECLARE @var1 int
		,@var2 int

SET @var1 = 0

SET @var1 = 0	-- won't work
	,@var2 = 0

SELECT 	@var1 = 0
		,@var2 = 0
------------------------------------------------------------


-- extract string from string with comma as delimiter
DECLARE @Stack VARCHAR(600)
		,@Ptr INT
		,@Ptr_Next INT
		,@Matched_Name VARCHAR(10)
SET @Stack = ', name1, name2, name3, '
SET @Ptr = 1

WHILE @Ptr <= LEN(@Stack)
BEGIN
	SET @Ptr_Next = CHARINDEX(', ', @Stack, @Ptr+2)

	IF @Ptr_Next = 0
		BREAK

	SET @Matched_Name = SUBSTRING(@Stack, @Ptr, @Ptr_Next-(@Ptr+2))
	PRINT @Matched_Name

	SET @Ptr = @Ptr_Next

END
------------------------------------------------------------



------------------------------------------------------------
----------------- Comparison & Type Convert ----------------
------------------------------------------------------------


-- how to convert a field from VARCHAR(n) to NTEXT
ALTER TABLE UserEvent ALTER COLUMN remark NTEXT NULL
------------------------------------------------------------


-- to compare a DateTime-type field with a literal datetime
SELECT * FROM table1 WHERE record_datetime > CONVERT(DATETIME , '2014-05-31' );
------------------------------------------------------------



------------------------------------------------------------
-------------------- Built-in functions --------------------
------------------------------------------------------------



-- how to get current date
UPDATE [User] SET login_date = GETDATE() WHERE guid = 'eeeee';
------------------------------------------------------------





/************************************************************
*************************************************************
******************** SYSTEM INFORMATION *********************
*************************************************************
************************************************************/



-- List all the tables names of the given database, may include sysdiagrams


-- No 1.
--C: Check constraint
--D: Default constraint
--F: Foreign Key constraint
--L: Log
--P: Stored procedure
--PK: Primary Key constraint
--RF: Replication Filter stored procedure
--S: System table
--TR: Trigger
--U: User table
--UQ: Unique constraint
--V: View
--X: Extended stored procedure
SELECT sobjects.name
FROM [dbo].[sysobjects] sobjects
WHERE sobjects.xtype = 'U'
------------------------------------------------------------


-- No 2, the name must be capital, and the reaults include Views
SELECT * FROM INFORMATION_SCHEMA.TABLES
------------------------------------------------------------


-- No 2, the results only include tables
SELECT '['+SCHEMA_NAME(schema_id)+'].['+name+']'
AS SchemaTable
FROM sys.tables
------------------------------------------------------------


-- List all the stored procedures
-- http://stackoverflow.com/questions/219434/query-to-list-all-stored-procedures
-- http://stackoverflow.com/questions/12622920/list-all-stored-procedures-with-schema-name
-- http://stackoverflow.com/questions/17417102/query-that-returns-list-of-all-stored-procedures-in-a-sql-server-database-which
SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE routine_type = 'PROCEDURE'
------------------------------------------------------------


-- If for some reason you had non-system stored procedures in the master database
SELECT *
FROM MASTER.INFORMATION_SCHEMA.ROUTINES
WHERE routine_type = 'PROCEDURE'
AND LEFT(Routine_Name, 3) NOT IN ('sp_', 'xp_', 'ms_')
------------------------------------------------------------


-- list all the columns in the current database

-- both the two catalogue views will list all the columns of tables & views
-- but sys.columns gives more than just the columns belonging to table/views
-- you created
-- and they provide differente information, like INFORMATION_SCHEMA.COLUMNS
-- directly provides DATA_TYPE.
SELECT * FROM sys.columns
SELECT * FROM INFORMATION_SCHEMA.COLUMNS

-- you may just want to get those belong to tables
SELECT *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE EXISTS (SELECT 1 FROM sys.tables WHERE tables.name = COLUMNS.TABLE_NAME)
------------------------------------------------------------


-- list all the indexes in the current database, you can find most of the options
-- setting which you use when creating indexes, like cluster/non-cluster, if it is
-- associated with a primary key, fill factor, etc.
SELECT * FROM sys.indexes
-- or you can inspect the indexes on a table
EXEC sp_helpindex '<table_name>'
------------------------------------------------------------


-- list all the relationship between index and columns in the current database
SELECT * FROM sys.index_columns
------------------------------------------------------------


-- list all the constraints in the current database
SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
-- or you can inspect the constraints on a table
EXEC sp_helpconstraint '<table_name>'
------------------------------------------------------------


-- list all the constraints information in the current database
SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
------------------------------------------------------------




--------------------------------------------
--------- Process each column in each ------
--------- table in the given db ------------
--------------------------------------------
USE AdventureWorks2008R2

DECLARE @tableName VARCHAR(50)
DECLARE @colName VARCHAR(50)

DECLARE @OutterCursor CURSOR

SET @OutterCursor = CURSOR FAST_FORWARD
FOR
SELECT name FROM sys.tables WHERE schema_id = 1

OPEN @OutterCursor

FETCH NEXT FROM @OutterCursor INTO @tableName

WHILE (@@FETCH_STATUS <> -1)
BEGIN

	PRINT 'Table: ' + @tableName

	DECLARE @MyInnerCursor CURSOR
	SET @MyInnerCursor = CURSOR FAST_FORWARD
	FOR
	SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName

	OPEN @MyInnerCursor
	FETCH NEXT FROM @MyInnerCursor INTO @colName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		PRINT 'Col: ' + @colName

		FETCH NEXT FROM @MyInnerCursor INTO @colName

	END

	CLOSE @MyInnerCursor
	DEALLOCATE @MyInnerCursor

	FETCH NEXT FROM @OutterCursor INTO @tableName

END

CLOSE @OutterCursor
DEALLOCATE @OutterCursor
------------------------------------------------------------




--------------------------------------------
--------- List Column Definition ------
---------  the given db ------------
--------------------------------------------
SELECT
	OBJECT_NAME(col.object_id) AS table_name
	,col.name AS col_name
	,ISCOL.ORDINAL_POSITION AS position
	,ISCOL.DATA_TYPE AS data_type
	,CASE
		WHEN ISCOL.DATA_TYPE = 'nchar'
			OR ISCOL.DATA_TYPE = 'nvarchar'
			OR ISCOL.DATA_TYPE = 'char'
			OR ISCOL.DATA_TYPE = 'varchar'
			-- OR ISCOL.DATA_TYPE = 'text'
			-- OR ISCOL.DATA_TYPE = 'ntext'
		THEN 'X(' + CAST(ISNULL(ISCOL.CHARACTER_OCTET_LENGTH, 0) AS VARCHAR(5)) + ')'
		WHEN ISCOL.DATA_TYPE = 'bigint'
			OR ISCOL.DATA_TYPE = 'int'
			OR ISCOL.DATA_TYPE = 'smallint'
		THEN '9(' + CAST(ISNULL(ISCOL.NUMERIC_PRECISION, 0) AS VARCHAR(5)) + ')'
		WHEN ISCOL.DATA_TYPE = 'decimal'
			OR ISCOL.DATA_TYPE = 'numeric'
		THEN '9(' + CAST(ISNULL(ISCOL.NUMERIC_PRECISION, 0) AS VARCHAR(5)) + ','
			+ CAST(ISNULL(ISCOL.NUMERIC_SCALE, 0) AS VARCHAR(5)) + ')'
		ELSE ''
	END AS [format]
	,CASE
		WHEN ISTC.CONSTRAINT_TYPE = 'PRIMARY KEY' THEN 'Y'
		ELSE ''
	END AS is_primary_key
FROM
	sys.columns col
	INNER JOIN INFORMATION_SCHEMA.COLUMNS ISCOL
		ON col.name = ISCOL.COLUMN_NAME
		   AND OBJECT_NAME(col.object_id) = ISCOL.TABLE_NAME
	LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE ISKCU
		ON col.name = ISKCU.COLUMN_NAME
		   AND OBJECT_NAME(col.object_id) = ISKCU.TABLE_NAME
	LEFT JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS ISTC
		ON ISKCU.TABLE_NAME = ISTC.TABLE_NAME
		   AND ISKCU.CONSTRAINT_NAME = ISTC.CONSTRAINT_NAME
		   AND ISKCU.CONSTRAINT_SCHEMA = ISTC.CONSTRAINT_SCHEMA
		   AND ISKCU.CONSTRAINT_CATALOG = ISTC.CONSTRAINT_CATALOG
WHERE
	col.object_id IN (SELECT object_id FROM sys.tables)
ORDER BY
	OBJECT_NAME(col.object_id), ISCOL.ORDINAL_POSITION
------------------------------------------------------------







/************************************************************
*************************************************************
******************* ADVANCED FUNCTIONS **********************
*************************************************************
************************************************************/



------------------------------------------------------------
--------------------- Temporary Tables ---------------------
------------------------------------------------------------


-- how to create a simplest temporary table and insert records
CREATE TABLE #tempTable
(
	field_a VARCHAR(50),
	field_b VARCHAR(100)
)
GO

INSERT INTO #tempTable (field_a, field_b) VALUES ('SM-T230', 'GALAXY Tab4'), ('SM-T230', 'SM-T230');
------------------------------------------------------------


-- complex operation with temporary table
CREATE TABLE #tempTable
(
	id INT
)
GO

INSERT INTO #prids
SELECT SomeFields
FROM SomeTable [JOIN <...> ON <...>]
[WHERE <...>]
...

DELETE FROM SomeTable
WHERE id IN (SELECT id FROM #tempTable);
------------------------------------------------------------


-- a convenient method to create a temporary table and insert records by select clause
-- with this approach, you don't have to create or declare the temp table beforehand
SELECT Field_A, Field_B
INTO #tempTable
FROM SomeTable [JOIN <...> ON <...>]
[WHERE <...>]
...
------------------------------------------------------------


-- some times, you may need the ordinal position of the rows in the temporary table
-- but somehow you happen not to have such thing in the source table(s),
-- in such case, you need a PK for the temporary table
CREATE TABLE #tempTable
(
	id INT IDENTITY(1,1) PRIMARY KEY,
	field_a VARCHAR(50) NOT NULL,
	field_b VARCHAR(100) NOT NULL
)
GO
------------------------------------------------------------


-- some developers use the following approach to simply clone a table with exactly the
-- same schema, without bothering to elaborate the columns definition
SELECT *
INTO #tempTable
FROM SomeTable
WHERE 1 = 0 --#tempTable has the same schema as SomeTable now
------------------------------------------------------------



------------------------------------------------------------
---------------------- Set Operation -----------------------
------------------------------------------------------------


-- how to find the intersection of two datasets
SELECT sua_id FROM Service
INTERSECT
SELECT sua_id FROM [User];  -- since 'user' is keyword
------------------------------------------------------------


-- to find the difference between two datasets
-- MIUNS is not supported by MS SQL Server, neither MySQL, you do that with 'Except'
SELECT sua_id FROM Service
EXCEPT
SELECT sua_id FROM [User];
------------------------------------------------------------





/************************************************************
*************************************************************
********************* DESIGN PATTERNS ***********************
*************************************************************
************************************************************/



-- check a variable's validity, you can use the convenient method:
-- you can avoid checking if it is NULL in another IF
IF ISNULL(@var, 0) = 0 -- for number variable
IF LEN(ISNULL(@var, '')) = 0 -- for string variable
------------------------------------------------------------


-- with a given @param, as the value for a field to perform
-- filtering in a query,
-- how to handle it more flexibly
SELECT <...>
FROM <...>
WHERE <...>
AND (
		ISNULL(@param, '') = ''
		OR
		(
			SomeField = @param
		)
	)
------------------------------------------------------------


-- how to catch error
-- https://msdn.microsoft.com/en-us/library/ms178592.aspx
BEGIN TRY
	...
	IF <...>
		GOTO Exitlabel
	...
Exitlabel:
END TRY

BEGIN CATCH

	SET @RETURNV = <...>;   -- output para

	DECLARE @ErrorNo INT,
			@Severity TINYINT,
			@State SMALLINT,
			@LineNo INT,
			@Message NVARCHAR(4000),
			@xact_state INT,
			@trancnt INT;

	SELECT @ErrorNo = ERROR_NUMBER(),
			@Severity = ERROR_SEVERITY(),
			@State = ERROR_STATE(),
			@LineNo = ERROR_LINE (),
			@Message = ERROR_MESSAGE(),
			@xact_state = XACT_STATE(),	--https://msdn.microsoft.com/en-us/library/ms189797.aspx
			@trancnt = @@TRANCOUNT;	--https://msdn.microsoft.com/en-us/library/ms187967.aspx

	RAISERROR(
				@Message,
				@Severity,
				@State
			);

END CATCH
------------------------------------------------------------


-- SELECT can be used to assign variables, and retrieve data from tables,
-- with the two functions combined together, you can fill in a table with partially data from another table easily
SELECT
Field_A = 'your value',
FieldInSrcTable_A, FieldInSrcTable_B [, ...]
INTO #TempTable
FROM SourceTable
[WHERE <...>]
------------------------------------------------------------


/*

Switchs
______________________
 TableName   | InUse
_____________|________
 Switcher_A  |  1
_____________|________
 Switcher_B  |  0
_____________|________

*/

CREATE VIEW Switcher
AS
SELECT * FROM Switcher_A
WHERE (
		SELECT InUse FROM Switchs
		WHERE TableName = 'Switcher_A'
	) = 1
UNION ALL
SELECT * FROM Switcher_B
WHERE (
		SELECT InUse FROM Switchs
		WHERE TableName = 'Switcher_B'
	) = 1

------------------------------------------------------------




-- field a
CREATE TABLE #field_a
(
	fa_value INT NOT NULL
)

INSERT INTO #field_a VALUES (1)
INSERT INTO #field_a VALUES (2)
INSERT INTO #field_a VALUES (3)

-- field b
CREATE TABLE #field_b
(
	fb_value INT NOT NULL
)
INSERT INTO #field_b VALUES (101)
INSERT INTO #field_b VALUES (102)
INSERT INTO #field_b VALUES (103)

-- field c
CREATE TABLE #field_c
(
	fc_value INT NOT NULL
)
INSERT INTO #field_c VALUES (188)
INSERT INTO #field_c VALUES (288)
INSERT INTO #field_c VALUES (388)


DECLARE @a INT
		,@b INT
		,@c INT

SELECT @a = NULL
		,@b = 1000
		,@c = 288

IF (
	( @a IS NULL OR EXISTS (SELECT 1 FROM #field_a WHERE fa_value = @a) )
	AND
	( @b IS NULL OR EXISTS (SELECT 1 FROM #field_b WHERE fb_value = @b) )
	AND
	( @c IS NULL OR EXISTS (SELECT 1 FROM #field_c WHERE fc_value = @b) )
)
	PRINT 'HIT'
ELSE
	PRINT 'NOT HIT'

------------------------------------------------------------



INSERT INTO #TABLEA
SELECT a, b, c, d, e
FROM
(
	SELECT 'x' AS b, 'aa' AS c
	UNION ALL
	SELECT 'y', 'bb'
	UNION ALL
	SELECT 'y', 'cc'
) AS tb1
CROSS JOIN
(
	SELECT 'll' AS d
	UNION ALL
	SELECT 'mm'
	UNION ALL
	SELECT 'nn'
) AS tb2
CROSS JOIN
(
	SELECT GETDATE() AS a, 0 AS e
) AS tb3
WHERE 1=1



------------------------------------------------------------
