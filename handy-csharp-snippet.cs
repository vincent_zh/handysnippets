/** 
 * Connect to database from ASP.NET
 * 
 * 
 */


/*
 * The backbone of the most primary way
 */

using System.Data.SqlClient;    // for SqlConnection, SqlCommand, SqlDataAdapter
using System.Data;  // for DataTable, DataSet

namespace yourNamespace
{
    public class yourClass
	{
        private string _connStr = "Data Source=USER-PC\\SQLEXPRESS; Integrated Security=True; Connect Timeout=15; Encrypt=False; TrustServerCertificate=False";
        private string _cmdStr = "SELECT * FROM Event";
		
		public object yourMethod()
		{
            SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand cmd = new SqlCommand(_cmdStr);
			SqlDataAdapter da = new SqlDataAdapter();
			DataTable dt = new DataTable();
			cmd.Connection = conn;
			da.SelectCommand = cmd;
			da.Fill(dt); 
		}
	}
}


/*
 * Or you can use a more compact method, to link them together
 */

namespace yourNamespace
{
    public class yourClass
	{
	    private string _connStr = "Data Source=USER-PC\\SQLEXPRESS; Integrated Security=True; Connect Timeout=15; Encrypt=False; TrustServerCertificate=False";
		private string _cmdStr = "SELECT * FROM Event";
		
		public object yourMethod()
		{
            SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand cmd = new SqlCommand(_cmdStr, conn);
			SqlDataAdapter da = new SqlDataAdapter(cmd);
			DataTable dt = new DataTable();

			da.Fill(dt); 
		}
	}
}

/*
 * A more safe and rational method is to enclose the statements with a try-catch block
 */

namespace yourNamespace
{
	public class yourClass
	{
		private string _connStr = "Data Source=USER-PC\\SQLEXPRESS; Integrated Security=True; Connect Timeout=15; Encrypt=False; TrustServerCertificate=False";
		private string _cmdStr = "SELECT * FROM Event";
		public object yourMethod()
		{
			try
			{
				SqlConnection conn = new SqlConnection(_connStr);
				SqlCommand cmd = new SqlCommand(_cmdStr, conn);
				SqlDataAdapter da = new SqlDataAdapter(cmd);
				DataTable dt = new DataTable();
				da.Fill(dt);
				objReslt.total_records = dt.Rows.Count;
			}
			catch (Exception ex)
			{
				objReslt.errmsg = ex.ToString();
			}
		}
	}
}


/*
 * The most modern approach in these days' practice
 */


namespace yourNamespace
{
	public class yourClass
	{
		private string _connStr = "Data Source=USER-PC\\SQLEXPRESS; Integrated Security=True; Connect Timeout=15; Encrypt=False; TrustServerCertificate=False";
		private string _cmdStr = "SELECT * FROM Event";
		public object yourMethod()
		{
			DataTable dt = new DataTable();
			
			using (SqlConnection conn = new SqlConnection(_connStr))
			using (SqlCommand cmd = new SqlCommand(_cmdStr, conn))
			using (SqlDataAdapter da = new SqlDataAdapter(cmd))
			{
				da.Fill(dt);
			}
		}
	}
}

// with using keyword, opening & closing the db connection is handled by framework automatically


/************************************************************************************************************************/


/*
 * Convert an arbitrary DB query result to JSONMediaTypeFormatter-serializable collection
 *
 */

using System.Collections.Generic;   // IDictionary
using System.Dynamic;   // for ExpandoObject

DataTable dt = new DataTable();

// suppose you have already filled the DataTable

var events = new List<object>();

foreach (DataRow dr in dt.Rows)
{
	dynamic eventObj = new ExpandoObject();

	foreach (DataColumn col in dt.Columns)
	{
		string field = col.ToString();
		string val = dr[col].ToString();

		((IDictionary<string, object>)eventObj).Add(field, val);
	}

	events.Add(eventObj);
}

// events now is ready for serialization in .NET MVC framework.


// of courese, if you can know the colmn names before hand, you can use Object

var events = new List<object>();

foreach (DataRow dr in dt.Rows)
{
	var eventObj = new object();

	eventObj = new
	{
		Id = dr["Id"],
		event_code = dr["event_code"],
		title = dr["title"]
	}

	events.Add(eventObj);
}

/************************************************************************************************************************/


		[HttpGet]
		public object check_session()
		{
			//throw new HttpResponseException(HttpStatusCode.NotFound);

			string suaId = clsSessAdminMgr.getSuaId(Request);
			if (string.IsNullOrEmpty(suaId))
			{
				var response = new HttpResponseMessage()
				{
					//StatusCode = (HttpStatusCode)422, // Unprocessable Entity
					StatusCode = HttpStatusCode.Unauthorized,
					ReasonPhrase = "Session Expire"
				};
				throw new HttpResponseException(response);
			}

			

			return new {
				timeout = "y"
			};
		}



/************************************************************************************************************************/


/*
 * The most basic & primary approach mentioned in textbook
 */
[HttpGet]
public object checkSession_CMPT()
{
	throw new HttpResponseException(HttpStatusCode.Unauthorized);
}

/****
req.status is: 401
stat is: error
err is: Unauthorized
**/


/*
 * You got exactly the same result as the previous one, nothing special happened so far
 */
[HttpGet]
public object checkSession_CMPT()
{
	var response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
	throw new HttpResponseException(response);
}

/****
req.status is: 401
stat is: error
err is: Unauthorized
**/


/*
 * With this method, you got more control over the content of response
 */
[HttpGet]
public object checkSession_CMPT()
{
	var token = "b80ab4e50448f6a5cf9249851b075efd";
	var response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
	{
		Content = new StringContent(string.Format("Token = {0}", token)),
		ReasonPhrase = "Session Expire"
	};
	throw new HttpResponseException(response);
}

/****
req.status is: 401
req.responseText is: Token = b80ab4e50448f6a5cf9249851b075efd
req.statusText is: Session Expire
stat is: error
err is: Session Expire 
**/


/*
 * Not quite different than the previous one, and need to find out how to specify statusText
 */
[HttpGet]
public object checkSession_CMPT()
{
	var token = "b80ab4e50448f6a5cf9249851b075efd";
	var message = String.Format("Token = {0}", token);
	var errorResponse = Request.CreateErrorResponse(HttpStatusCode.Unauthorized, message);
	throw new HttpResponseException(errorResponse);
}

/****
req.status is: 401
req.responseText is: {"Message":"Token = b80ab4e50448f6a5cf9249851b075efd"}
req.statusText is: Unauthorized
stat is: error
err is: Unauthorized
**/


/*
 * Return the HttpResponseMessage containing httpError directly
 */
[HttpGet]
public HttpResponseMessage checkSession_CMPT()
{
	var token = "b80ab4e50448f6a5cf9249851b075efd";
	var message = String.Format("Token = {0}", token);
	var httpError = new HttpError(message);
	return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, httpError);
}

/****
req.status is: 401
req.responseText is: {"Message":"Token = b80ab4e50448f6a5cf9249851b075efd"}
req.statusText is: Unauthorized
stat is: error
err is: Unauthorized 
**/




/************************************************************************************************************************/

[HttpPut]
public object updateGift_WRAP(int id, IM_Gift gift)
{
    //...
}

[HttpPost]
public object createGift_WRAP([FromBody] IM_Gift gift)
{
	//..
}

// if you code the API in this way, then the Ajax should be:

/****
var gift = {
	title_tc : 'test_update_stringfity',
	title_eng : 'test_update_stringfity',
	HTML_eng : 'test_update_stringfity',
	HTML_tc : 'test_update_stringfity',
	quantityInHand : 2,
	realPrice : 25.2
};

$.ajax({
	url : '/api/auction/updategift_wrap/5',
	type : 'PUT',
	data : JSON.stringify(gift),
	contentType: "application/json;charset=utf-8"
}).done(function updateGift_doneHandler(res) {
	console.log(res);
}).fail( function updateGift_failHandler(req, stat, err){
	console.log(req);
	console.log(stat);
	console.log(err);
});
**/

// or

/****
$.ajax({
	url : '/api/auction/updategift_wrap/5',
	type : 'PUT',
	data : {
		title_tc : 'test_update_stringfity',
		title_eng : 'test_update_stringfity',
		HTML_eng : 'test_update_stringfity',
		HTML_tc : 'test_update_stringfity',
		quantityInHand : 2,
		realPrice : 25.2
	}
}).done(function updateGift_doneHandler(res) {
	console.log(res);
}).fail( function updateGift_failHandler(req, stat, err){
	console.log(req);
	console.log(stat);
	console.log(err);
});
**/

/************************************************************************************************************************/


/*
 * Date & Time Calculation
 */

// user is a record from DB, try_login_date is Datetime
var date1 = DateTime.Now.Date;
var date2 = ((DateTime)user.try_login_date).Date;

int result = DateTime.Compare(date1, date2);

if (0 == result)
{
	
}

// identifiedUser is a record from DB, password_timestamp is of Datetime type
var lastPwdReset = (DateTime)identifiedUser.password_timestamp;
TimeSpan elapsed = DateTime.Now.Subtract(lastPwdReset);

// pwdVidtyPrd is int
if (elapsed.TotalDays > pwdVidtyPrd)
{
	
}



/************************************************************************************************************************/


static void DoSomething(ref string s1)
{
	Console.WriteLine(s1);
	s1 += " new string appended";
	Console.WriteLine(s1);
}

static void Main(string[] args)
{
	// passing parameters by reference
	string inMain = "In Main";
	Console.WriteLine(inMain);
	DoSomething(ref inMain);
	Console.WriteLine(inMain);
}

/************************************************************************************************************************/



/*
 * LINQ to Entities, in EF 4.0, 5.0, 6.0
 */

// return entire entities
var contacts = from c in context.Contacts
				where c.FirstName == "Robert"
				select c;

// string comparision of ntext field
var events = from e in this.objEntity.Events
			 where e.html_tc.Contains("odr2014")
			 select e;

// return projection of entities, with anonymous type
var events = from e in this.objEntity.Events
			 where e.html_tc.Contains("odr2014")
			 select new { 
				 e.id, 
				 e.end_date
			 };


// return projection of entities, with named anonymous type
var events = from e in this.objEntity.Events
			 where e.html_tc.Contains("odr2014")
			 let EventDetail = new { 
				 e.id, 
				 e.event_code
			 }
			 select EventDetail;

// return projection of entities, with assocations


// In order to prevent System.InvalidOperationException, you need to add this line to WebApiConfig.cs:
// config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;


var events = from a in this.objEntity.RegistrationApproveRecords
				where a.status == 1
				select new { a, a.ProductRegistration };



var events = from pr in this.objEntity.ProductRegistrations
			 where pr.id < 383
			 let foo = new
			 {
				 ProdReg = new { pr.model_code, pr.purchase_price },
				 pr.RegistrationApproveRecords
			 }
			 orderby foo.ProdReg.model_code
			 select foo;
			 
			 
var events = from pr in this.objEntity.ProductRegistrations
			 select new
			 {
				 pr.id,
				 pr.model_name,
				 Approves = from a in pr.RegistrationApproveRecords
							select new { a.sua_id, a.lastModifiedBy }
			 };			 



/*
 * Grouping
 */
var events = from e in this.objEntity.Events
			 group e by e.event_code into MyGroup
			 select MyGroup;


var events = from e in this.objEntity.Events
			 group e by e.event_code into MyGroup
			 orderby MyGroup.Key
			 select new { MyTitle = MyGroup.Key, MyGroup };

// 
var events = from e in this.objEntity.Events
			 group e by e.event_code into MyGroup
			 orderby MyGroup.Key
			 select new
			 {
				 MyTitle = MyGroup.Key,
				 MyGroup,
				 Max = MyGroup.Max(c => c.redeem_point),
				 Count = MyGroup.Count()
			 };

// grouping & aggregation
var events = from e in this.objEntity.Events
			 group e by e.event_code into MyGroup
			 where MyGroup.Count() > 2
			 select new
			 {
				 MyTitle = MyGroup.Key,
				 MyGroup,
				 Max = MyGroup.Max(c => c.redeem_point),
				 Count = MyGroup.Count()
			 };
// filtering by group condition
var events = from e in this.objEntity.Events
			 let c = new { e.id, e.event_code, e.html_tc }
			 group c by c.event_code into MyGroup
			 where MyGroup.Count() > 2
			 select MyGroup;


// Join

var events = from bi in this.objEntity.bid_item
			 join g in this.objEntity.gifts on bi.GiftId equals g.Id
			 where bi.AskPoint > 1000 && bi.Status == "1"
			 select new
			 {
				 bi.AskPoint,
				 g.Title_tc
			 };


// nested query

var events = from bi in this.objEntity.bid_item
			 select new
			 {
				 bi.AskPoint,
				 gift = (from g in this.objEntity.gifts
						 where g.Id == bi.GiftId
						 select g
						 ).FirstOrDefault()
			 };








/*
 * LINQ methods & lambda expression Or is it Entity SQL query builder method????????????????
 */

// return projection of entities
var events = this.objEntity.Events
			.Where(e => e.event_code == "s2000")
			.Select(e => new { e.id, e.html_tc });




/*
 * Entity SQL, in EF 4.0
 */

// Object Services & Entity SQL
string query = "SELECT VALUE e " +
				"FROM samsunginternalEntities.Events AS e " +
				"WHERE e.event_code = 's2000' ORDER BY e.id";
ObjectQuery<Event> events = this.objEntity.CreateQuery<Event>(query);




				
/*
 * Entity SQL, in EF 5.0, 6.0
 */

var queryString = "SELECT VALUE c " +
					"FROM PROGRAMMINGEFDB1Entities.Contact AS c " +
					"WHERE c.FirstName='Robert'";
ObjectQuery<Contact> contacts = ((IObjectContextAdapter)context).ObjectContext.CreateQuery<Contact>(queryString);


var queryString = "SELECT VALUE c.LastName " +
					"FROM PROGRAMMINGEFDB1Entities.Contact AS c " +
					"WHERE c.FirstName='Robert'";
ObjectQuery<string> contacts = ((IObjectContextAdapter)context).ObjectContext.CreateQuery<string>(queryString);






/*
 * parameterized query with System.Data.Objects.ObjectContext.ExecuteStoreQuery<T>()
 */

var filters = new StringBuilder();
var parameters = new List<object>();

string strUsageTag = "auction";
string strTitle_en = "%test%";

filters.Append("UsageTag = @UsageTag AND Title_en LIKE @Title_en");

var paramUsageTag = new SqlParameter("@UsageTag", SqlDbType.VarChar);
paramUsageTag.Value = strUsageTag;
parameters.Add(paramUsageTag);

var paramTitle_en = new SqlParameter("@Title_en", SqlDbType.NVarChar);
paramTitle_en.Value = strTitle_en;
parameters.Add(paramTitle_en);

string sqlStatement = "SELECT * FROM gift WHERE ";

sqlStatement = sqlStatement + filters;

List<OM_Gift_BO> gifts = this.objEntity.ExecuteStoreQuery<OM_Gift_BO>(sqlStatement, parameters.ToArray()).ToList();

foreach (OM_Gift_BO record in gifts)
{
	Trace.WriteLine("Title_en: " + record.Title_en);
}


